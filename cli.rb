require 'optparse'
require_relative 'autoload.rb'
require_relative 'app.rb'

class CLI
  def run
    ARGV << '-h' if ARGV.empty?

    OptionParser.new do |parser|
      parser.banner = 'Usage: ruby cli.rb -p path/to/file'

      parser.on('-p PATH', '--path PATH', 'Path to customers.txt input file') do |path|
        App.new(input_file_path: path).call
      end

      parser.on_tail('-h', '--help', 'Help guide') do
        puts parser
        exit
      end
    end.parse!
  end
end

CLI.new.run
