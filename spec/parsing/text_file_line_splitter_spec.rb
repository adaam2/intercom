RSpec.describe Parsing::TextFileLineSplitter do
  let(:path) { '/fake/path/' }
  subject { described_class.new(path) }

  describe '#call' do
    context 'When the file at path exists' do
      let(:lines) do
        ['{ "user_id": 1 }', '{ "user_id": 2 }']
      end

      before do
        allow(File)
          .to receive(:exist?)
          .with(path)
          .and_return(true)

        allow(File)
          .to receive(:readlines)
          .and_return(lines)
      end

      it 'returns the correct data' do
        expect(subject.call)
          .to eq lines
      end

      it 'calls readlines' do
        subject.call

        expect(File)
          .to have_received(:readlines)
          .with(path)
          .once
      end

      context 'When the input file has blank lines in it' do
        let(:lines) do
          ['', nil, '{ "user_id": 1 }']
        end

        it 'does not include nil or blank lines' do
          expect(subject.call)
            .to eq ['{ "user_id": 1 }']
        end
      end
    end

    context 'When the file at the path does not exist' do
      it 'raises an File does not exist error' do
        expect {
          subject
        }.to raise_error('File does not exist')
      end
    end
  end
end
