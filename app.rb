require 'colorize'
require 'pry'

class App
  def initialize(input_file_path:)
    @input_file_path = File.join(__dir__, input_file_path)
  end

  def call
    customers, invalid_customers = parse_lines
                                   .map { |line| json_parser.call(line) }
                                   .map { |json| customer_parser.call(json) }
                                   .partition(&:valid?)

    eligible_customers = Queries::FilterAndOrder.new(customers: customers).call

    log_eligible_customers(eligible_customers.within_radius)

    log_ineligible_customers(eligible_customers.outside_radius)

    log_invalid_customers(invalid_customers)
  end

  private

  attr_reader :input_file_path

  def parse_lines
    Parsing::TextFileLineSplitter.new(input_file_path).call
  rescue StandardError => e
    puts e.message
    exit
  end

  def log_eligible_customers(customers)
    return if customers.empty?

    log "#{double_newline}The customers located within #{max_distance} kilometre radius are:#{double_newline}"
    log(customers.map { |c| c.to_s(true).green })
  end

  def log_ineligible_customers(customers)
    return if customers.empty?

    log "#{double_newline}The customers that were excluded due to being over the maximum distance were:#{double_newline}"

    log(customers.map { |c| c.to_s(true).red })
  end

  def log_invalid_customers(customers)
    return if customers.empty?

    log "#{double_newline}The customers excluded due to invalid input data were:#{double_newline}"

    log(customers.map { |c| "#{c} #{c.errors.full_messages}".red })
  end

  def max_distance
    @max_distance ||= Constants::MAX_DISTANCE_IN_KM
  end

  def json_parser
    @json_parser ||= Parsing::JSONParser.new
  end

  def customer_parser
    @customer_parser ||= Parsing::JSONToCustomer.new
  end

  def log(message)
    puts message
  end

  def double_newline
    "\n\n"
  end
end
