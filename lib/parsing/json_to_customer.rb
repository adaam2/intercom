module Parsing
  class JSONToCustomer
    def call(json_object)
      customer = Customer.from_json(json_object)

      begin
        customer.distance = calculate_distance.call(
          lat1: customer.latitude,
          lon1: customer.longitude,
          lat2: intercom_office_coordinates[0],
          lon2: intercom_office_coordinates[1]
        )
      rescue
        puts "Could not calculate distance for #{customer.name} as lat/lng values were missing."
      end

      customer
    end

    private

    def intercom_office_coordinates
      Constants::INTERCOM_COORDINATES
    end

    def calculate_distance
      @calculate_distance ||= Geolocation::CalculateDistance.new
    end
  end
end
