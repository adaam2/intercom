module Parsing
  class TextFileLineSplitter
    attr_reader :path

    def initialize(path_to_file)
      @path = path_to_file
      raise 'File does not exist' unless File.exist?(@path)
    end

    def call
      lines.select(&:presence)
    end

    private

    def lines
      File.readlines path
    end
  end
end
