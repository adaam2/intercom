require 'json'

module Parsing
  class JSONParser
    def call(json_object_string)
      raw_json = JSON.parse(json_object_string)
      convert_value_types(raw_json)
    end

    private

    def convert_value_types(json)
      begin
        json['latitude'] = Float(json['latitude'])
      rescue TypeError, ArgumentError
        puts "Invalid latitudinal value for #{json['latitude']}"
      end

      begin
        json['longitude'] = Float(json['longitude'])
      rescue TypeError, ArgumentError
        puts "Invalid longitudinal value for #{json['longitude']}"
      end

      json
    end
  end
end
