require 'active_model'

class Customer
  include ActiveModel::Model

  attr_accessor :user_id, :latitude, :longitude, :name, :distance

  validates_presence_of :user_id, :latitude, :longitude, :name

  validates_numericality_of :user_id, only_integer: true, message: 'ID must be a number. e.g. 1234'
  validates_numericality_of :latitude, greater_than_or_equal_to:  -90, less_than_or_equal_to:  90, message: 'must be within valid range of -90 to 90. Value was %{value}'
  validates_numericality_of :longitude, greater_than_or_equal_to: -180, less_than_or_equal_to: 180, message: 'must be within valid range of -180 to 180. Value was %{value}'

  def self.from_json(json)
    new(json)
  end

  def to_s(include_distance = false)
    ret_val = "#{name} - ID #{user_id}"
    ret_val += " (roughly #{distance.round} kilometres away)" if include_distance
    ret_val
  end
end
