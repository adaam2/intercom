module Geolocation
  class CalculateDistance
    def call(lat1:, lon1:, lat2:, lon2:)
      raise 'Please provide valid coordinates' unless all_coordinates_present?(lat1, lon1, lat2, lon2)
      Math.acos(
        Math.sin(to_radians(lat1)) * Math.sin(to_radians(lat2)) +
        Math.cos(to_radians(lat1)) * Math.cos(to_radians(lat2)) *
        Math.cos(to_radians(lon2 - lon1))
      ) * Constants::RADIUS_OF_EARTH
    end

    private

    def all_coordinates_present?(*args)
      args.all?(&:present?)
    end

    def to_radians(degrees)
      degrees * Math::PI / 180.0
    end
  end
end
